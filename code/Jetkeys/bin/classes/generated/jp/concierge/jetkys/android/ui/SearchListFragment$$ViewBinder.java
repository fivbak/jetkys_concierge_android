// Generated code from Butter Knife. Do not modify!
package jp.concierge.jetkys.android.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SearchListFragment$$ViewBinder<T extends jp.concierge.jetkys.android.ui.SearchListFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099676, "field 'mListView'");
    target.mListView = finder.castView(view, 2131099676, "field 'mListView'");
  }

  @Override public void unbind(T target) {
    target.mListView = null;
  }
}
