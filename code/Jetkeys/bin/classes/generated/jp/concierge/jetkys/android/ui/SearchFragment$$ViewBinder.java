// Generated code from Butter Knife. Do not modify!
package jp.concierge.jetkys.android.ui;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SearchFragment$$ViewBinder<T extends jp.concierge.jetkys.android.ui.SearchFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131099726, "field 'mGenderRgroup'");
    target.mGenderRgroup = finder.castView(view, 2131099726, "field 'mGenderRgroup'");
    view = finder.findRequiredView(source, 2131099733, "method 'searchClick'");
    view.setOnClickListener(
      new butterknife.internal.DebouncingOnClickListener() {
        @Override public void doClick(
          android.view.View p0
        ) {
          target.searchClick();
        }
      });
    target.mTypeCbList = Finder.listOf(
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099792, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099793, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099794, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099795, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099796, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099797, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099798, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099799, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099800, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099801, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099802, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099803, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099804, "field 'mTypeCbList'"),
        finder.<android.widget.CheckBox>findRequiredView(source, 2131099805, "field 'mTypeCbList'")
    );
  }

  @Override public void unbind(T target) {
    target.mGenderRgroup = null;
    target.mTypeCbList = null;
  }
}
