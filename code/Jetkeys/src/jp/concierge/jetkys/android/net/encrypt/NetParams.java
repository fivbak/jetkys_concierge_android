package jp.concierge.jetkys.android.net.encrypt;

public interface NetParams {
	public String DID="did";
	public String IV="iv";
	public String TOKEN="token";
	public String DATETIME="datetime";
	public String REQUEST_NO="request_no";
	public String DEVICE_TYPE="device_type";
	public String DATA="data";
    public String ID="id";

	public  final String REGISTER_URL = "{\"create_user\": {\"nickname\": %s,\"token\":%s}}";
	public  final String TOKENURL="%s:%s:%s";
	public final String LOGIN_URL="{\"auth\": {\"token\": %s}}";
	public final String GET_GOSSIP_CATEGORY_LIST="{\"get_gossip_category_list\":null}";
}