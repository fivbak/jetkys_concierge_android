package jp.concierge.jetkys.android.manager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;
import jp.concierge.jetkys.android.net.ProtocolConstant;

/**
 * Created by zhuwenjin on 15/9/18.
 */
public abstract class BaseManager {
    protected Context mContext;

    public void onCreate(Context context) {
        mContext = context;
    }

    public void newOne() {
        isDestoryed = false;
    }

    private boolean isDestoryed;

    public void onDestroy() {
        isDestoryed = true;
    }

    public <T extends BaseManager> T getManager(Context context, Class<? extends BaseManager> ownerClass) {
        return (T) ManagerFactory.getInstance().getManager(context, ownerClass);
    }

    protected boolean commonSuccessCheck() {
        if (isDestoryed) return false;

        return true;
    }

    protected boolean commonFailedCheck(String errCode, String errMsg, boolean needToast) {
        if (isDestoryed) return false;

//        if (ProtocolConstant.ERR_CODE_NOT_LOGIN.equals(errCode)) {
//            clearUserDatas();
//
//            toast(errMsg);
//            Intent intent = new Intent(mContext, LoginActivity.class);
//            mContext.startActivity(intent);
//        }

        if (needToast) {
            toast(errMsg);
        }

        return true;
    }

//    protected void clearUserDatas() {
//        SharedPreferences cookieSp = mContext.getSharedPreferences(HttpClientRequest.COOKIE, Context.MODE_PRIVATE);
//        cookieSp.edit().clear().commit();
//    }

    protected void toast(CharSequence s) {
        Toast.makeText(mContext, s, Toast.LENGTH_LONG).show();
    }
}
