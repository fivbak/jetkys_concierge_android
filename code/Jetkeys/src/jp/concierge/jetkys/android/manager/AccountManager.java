package jp.concierge.jetkys.android.manager;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.SharedPreferenceHelper;
import jp.concierge.jetkys.android.beans.LoginBean;
import jp.concierge.jetkys.android.gameutils.encrypt.EncryptData;
import jp.concierge.jetkys.android.net.http.ActionListener;
import jp.concierge.jetkys.android.net.http.HttpAction;
import jp.concierge.jetkys.android.net.http.client.HttpMethodType;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.utils.CryptUtil;

/**
 * Created by WilliZ on 2015/9/23.
 */
public class AccountManager extends BaseManager {
    public enum LOGIN_TYPE {
        EMAIL("1"), TWITTER("2"), FACEBOOK("3"), GOOGLEP("4");

        LOGIN_TYPE(String id) {
            this.id = id;
        }

        public String id;
    }

    private static final String COMMON_SP = "common.xml";
    private static final String SP_COMPANY_NAME = "cname";
    private static final String SP_USER_NAME = "uname";
    private static final String SP_MOBILE = "mobile";

    private ProtocolManager mProtocolManager;

//    private ProfileEntity mProfile;
//
//    public ProfileEntity getProfile() {
//        return mProfile;
//    }

    public String user_id = "";
    public String install_key = "";
    public String concierge_id = "";
    public String nickname = "";
    public String profile_image_url = "";

    @Override
    public void onCreate(Context context) {
        super.onCreate(context);
        mProtocolManager = getManager(context, ProtocolManager.class);

        user_id = SharedPreferenceHelper.getString("user_id");
//        user_id = "76";
        install_key = SharedPreferenceHelper.getString("install_key");
        concierge_id = SharedPreferenceHelper.getString("concierge_id");
        nickname = SharedPreferenceHelper.getString("nickname");
        profile_image_url = SharedPreferenceHelper.getString("profile_image_url");
    }

    public boolean isLogined() {
        return !TextUtils.isEmpty(user_id);
    }

    private void saveLoginStatus(boolean autoLogin, LoginBean result) {
        user_id = result.id;
        install_key = result.install_key;
        concierge_id = result.concierge_id;
        nickname = result.nickname;
        profile_image_url = result.profile_image_url;

        Settings.save(Settings.PAY_CONFIRM_SEND_MAIL,
                "1".equals(result.setting_image));
        Settings.save(Settings.PAY_CONFIRM_SEE_PICTURE,
                "1".equals(result.setting_mail));
        Settings.save(Settings.PAY_CONFIRM_SEND_MESSAGE,
                "1".equals(result.setting_message));

        if (autoLogin) {
            SharedPreferenceHelper.putString("user_id", result.id);
            SharedPreferenceHelper.putString("install_key", result.install_key);
            SharedPreferenceHelper.putString("concierge_id", result.concierge_id);
            SharedPreferenceHelper.putString("nickname", result.nickname);
            SharedPreferenceHelper.putString("profile_image_url", result.profile_image_url);
        }
    }

    public void clearLoginstatus() {
        SharedPreferenceHelper.clear();

        this.user_id = "";
        this.install_key = "";
        this.concierge_id = "";
        this.nickname = "";
        this.profile_image_url = "";
    }

    public void setImageUrl(String imgUrl) {
        this.profile_image_url = imgUrl;
        SharedPreferenceHelper.putString("profile_image_url", imgUrl);
    }

    public void setNickName(String nick) {
        this.nickname = nick;
        SharedPreferenceHelper.putString("nickname", nick);
    }

    public void registerViaEmail(String unick,
                                 String email,
                                 String password,
                                 String gender,
                                 String birth,
                                 final Callback<LoginBean> cb) {
        HttpAction action = new HttpAction(HttpMethodType.REGISTER);
        action.putJson("nickname", unick);
        action.putJson("gender", gender);
        action.putJson("birth", birth);
        StringBuilder sb = new StringBuilder();
        sb.append(LOGIN_TYPE.EMAIL.id);
        sb.append(":");
        sb.append(email);
        sb.append(":");
        sb.append(password);
        sb.append(":");
        sb.append(CryptUtil.getDateTime());

//        String IVStr = EncryptData.getIV();
//        String token = EncryptData.encrypt(sb.toString(), IVStr);
//        action.putJson("token", token);
        action.putJson("token", sb.toString());

        action.setActionListener(new ActionListener<LoginBean>() {
            @Override
            public void onSuccess(LoginBean result) {
                if (cb != null && commonSuccessCheck()) {
                    saveLoginStatus(true, result);
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithDid(action);
    }

    public void registerViaThirdParty(String unick,
                                      LOGIN_TYPE type,
                                      String accessToken,
                                      final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.REGISTER);
        action.putParam("nickname", unick);
        StringBuilder sb = new StringBuilder();
        sb.append(type.id);
        sb.append(":");
        sb.append(accessToken);
        sb.append(":");
        sb.append(CryptUtil.getDateTime());

        String IVStr = EncryptData.getIV();
        String token = EncryptData.encrypt(sb.toString(), IVStr);
        action.putJson("token", token);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithDid(action);
    }

    public void loginViaEmail(final boolean autoLogin,
                              String email, String password,
                              final Callback<LoginBean> cb) {
        HttpAction action = new HttpAction(HttpMethodType.LOGIN);
        StringBuilder sb = new StringBuilder();
        sb.append(LOGIN_TYPE.EMAIL.id);
        sb.append(":");
        sb.append(email);
        sb.append(":");
        sb.append(password);
        sb.append(":");
        sb.append(CryptUtil.getDateTime());

//        String IVStr = EncryptData.getIV();
//        String token = EncryptData.encrypt(sb.toString(), IVStr);
//        action.putJson("token", token);
        action.putJson("token", sb.toString());

        action.setActionListener(new ActionListener<LoginBean>() {
            @Override
            public void onSuccess(LoginBean result) {
                if (cb != null && commonSuccessCheck()) {
                    saveLoginStatus(autoLogin, result);
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithDid(action);
    }

    public void uploadPushToken(String pushToken,
                                final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.SAVE_PUSH);
        action.putJson("push_id", pushToken);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }
}
