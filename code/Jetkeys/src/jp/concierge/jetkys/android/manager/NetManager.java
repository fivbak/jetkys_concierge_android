package jp.concierge.jetkys.android.manager;

import android.content.Context;
import jp.concierge.jetkys.android.beans.*;
import jp.concierge.jetkys.android.beans.base.*;
import jp.concierge.jetkys.android.net.http.ActionListener;
import jp.concierge.jetkys.android.net.http.HttpAction;
import jp.concierge.jetkys.android.net.http.client.HttpMethodType;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.utils.PhotoUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by WilliZ on 16/03/24.
 */
public class NetManager extends BaseManager {
    public static final int PAGE_NUM = 20;
    public static final int PAGE_FIRST = 1;

    private ProtocolManager mProtocolManager;

    @Override
    public void onCreate(Context context) {
        super.onCreate(context);
        mProtocolManager = getManager(context, ProtocolManager.class);
    }

    public static final String PARAM_START = "start_number";
    public static final String PARAM_END = "end_number";

    public void uploadPhoto(String photoPath,
                            final Callback<UploadPhotoBean> cb) {
        HttpAction action = new HttpAction(HttpMethodType.UPLOAD_IMAGE);
        action.putParam("file0", PhotoUtils.genUploadPhotoParam(photoPath));

        action.setActionListener(new ActionListener<UploadPhotoBean>() {
            @Override
            public void onSuccess(UploadPhotoBean result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void setUserImage(String type, String photoPath,
                             final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.SET_USER_IMAGE);
        action.putJson("image", photoPath);
        action.putJson("type", type);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getMailInboxList(int start,
                                 final Callback<BaseMailListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_MAIL_INBOX_LIST);
        action.putJson(PARAM_START, start);
        action.putJson(PARAM_END, start + PAGE_NUM);

        action.setActionListener(new ActionListener<BaseMailListEntity>() {
            @Override
            public void onSuccess(BaseMailListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getMailSendList(int start,
                                final Callback<BaseMailListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_MAIL_SEND_LIST);
        action.putJson(PARAM_START, start);
        action.putJson(PARAM_END, start + PAGE_NUM);

        action.setActionListener(new ActionListener<BaseMailListEntity>() {
            @Override
            public void onSuccess(BaseMailListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getMailFriendList(String uid, String cid,
                                  int start,
                                  final Callback<BaseMailListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_MAIL_INFO_LIST);
        action.putJson("concierge_id", cid);
        action.putJson("user_id", uid);
        action.putJson(PARAM_START, start);
        action.putJson(PARAM_END, start + PAGE_NUM);

        action.setActionListener(new ActionListener<BaseMailListEntity>() {
            @Override
            public void onSuccess(BaseMailListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getMailDetail(String id,
                              final Callback<MailBean> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_MAIL);
        action.putJson("id", id);

        action.setActionListener(new ActionListener<MailBean>() {
            @Override
            public void onSuccess(MailBean result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void sendMail(String cid,
                         String title,
                         String message,
                         String attachment,
                         String conversation_id,
                         String diagnoses_id,
                         final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.SEND_MAIL);
        action.putJson("concierge_id", cid);
        action.putJson("title", title);
        action.putJson("message", message);
        action.putJson("attachment", attachment);
        action.putJson("conversation_id", conversation_id);
        action.putJson("diagnoses_id", diagnoses_id);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getNoticeList(int start,
                              int end,
                              final Callback<BaseNoticeListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_NOTICE_LIST);
        action.putJson(PARAM_START, start);
        action.putJson(PARAM_END, end);

        action.setActionListener(new ActionListener<BaseNoticeListEntity>() {
            @Override
            public void onSuccess(BaseNoticeListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getNoticeDetail(String id,
                                final Callback<SysMessage> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_NOTICE);
        action.putJson("id", id);

        action.setActionListener(new ActionListener<SysMessage>() {
            @Override
            public void onSuccess(SysMessage result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    /**
     * 获取专家列表
     *
     * @param categories   "categories": "1,2,3,4,5,6,7"
     * @param type         "type": "0" // 0-全部，1-我的
     * @param gender       "gender": "1" // 1-男，2-女
     * @param start_number "start_number":1
     * @param end_number   "end_number" : 20
     * @param cb
     */
    public void getConciergeList(String categories,
                                 String type,
                                 String gender,
                                 int start_number,
                                 int end_number,
                                 String cid,
                                 final Callback<BaseConciergeListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_CONCIERGE_LIST);
        action.putJson("categories", categories);
        action.putJson("type", type);
        action.putJson("gender", gender);
        action.putJson("start_number", start_number);
        action.putJson("end_number", end_number);
        action.putJson("user_concierge_id", cid);

        action.setActionListener(new ActionListener<BaseConciergeListEntity>() {
            @Override
            public void onSuccess(BaseConciergeListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithDid(action);
    }

    public void getConciergeDetail(String id,
                                   final Callback<Concierge> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_CONCIERGE);
        action.putJson("id", id);

        action.setActionListener(new ActionListener<Concierge>() {
            @Override
            public void onSuccess(Concierge result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getDiagnosisList(String concierge_id,
                                 final Callback<BaseDiagnosisListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_DIAGNOSIS_LIST);
        action.putJson("concierge_id", concierge_id);

        action.setActionListener(new ActionListener<BaseDiagnosisListEntity>() {
            @Override
            public void onSuccess(BaseDiagnosisListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getDiagnosis(String id,
                             final Callback<Diagnosis> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_DIAGNOSIS);
        action.putJson("id", id);

        action.setActionListener(new ActionListener<Diagnosis>() {
            @Override
            public void onSuccess(Diagnosis result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getFriendList(final Callback<BaseFriendListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_FRIEND_LIST);

        action.setActionListener(new ActionListener<BaseFriendListEntity>() {
            @Override
            public void onSuccess(BaseFriendListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getUserInfo(String type,
                            final Callback<UserInfo> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_USER_INFO);
        action.putJson("type", type);

        action.setActionListener(new ActionListener<UserInfo>() {
            @Override
            public void onSuccess(UserInfo result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getEditUser(String type,
                            final Callback<EditUserInfo> cb) {
        HttpAction action = new HttpAction(HttpMethodType.EDIT_USER);
        action.putJson("type", type);

        action.setActionListener(new ActionListener<EditUserInfo>() {
            @Override
            public void onSuccess(EditUserInfo result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void uploadUser(String type,
                           String nickname,
                           String birthday,
                           String gender,
                           String categories,
                           final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.UPLOAD_USER);
        action.putJson("type", type);
        action.putJson("gender", gender);
        action.putJson("nickname", nickname);
        action.putJson("birthday", birthday);
        action.putJson("category_name", categories);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getConversationList(final Callback<List<Conversation>> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_CONVERSATION_LIST);

        action.setActionListener(new ActionListener<List<Conversation>>() {
            @Override
            public void onSuccess(List<Conversation> result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void getChatMsgList(String diagnosis_id,
                               String concierge_id,
                               String user_id,
                               int start_number,
                               final Callback<BaseMessageListEntity> cb) {
        HttpAction action = new HttpAction(HttpMethodType.GET_CHATMSG_LIST);
        action.putJson("diagnosis_id", diagnosis_id);
        action.putJson("concierge_id", concierge_id);
        action.putJson("user_id", user_id);
        action.putJson("start_number", start_number);
        action.putJson("end_number", start_number + PAGE_NUM);

        action.setActionListener(new ActionListener<BaseMessageListEntity>() {
            @Override
            public void onSuccess(BaseMessageListEntity result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void sendMessage(String conversation_id,
                            String attachment,
                            String concierge_id,
                            String message,
                            String user_id,
                            final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.SEND_MESSAGE);
        action.putJson("attachment", attachment);
        action.putJson("concierge_id", concierge_id);
        action.putJson("message", message);
        action.putJson("user_id", user_id);
        action.putJson("conversation_id", conversation_id);

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void seeMessage(String id,
                           final Callback<Message> cb) {
        HttpAction action = new HttpAction(HttpMethodType.ENTER_MESSAGE);
        action.putJson("id", id);

        action.setActionListener(new ActionListener<Message>() {
            @Override
            public void onSuccess(Message result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }

    public void submitSetting(String key, boolean value,
                              final Callback<Void> cb) {
        HttpAction action = new HttpAction(HttpMethodType.SUBMIT_SETTING);
        action.putJson(key, value ? "1" : "0");

        action.setActionListener(new ActionListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                if (cb != null && commonSuccessCheck()) {
                    cb.notify(result, true);
                }
            }

            @Override
            public void onFailure(String errCode, String errMsg) {
                if (cb != null && commonFailedCheck(errCode, errMsg, true)) {
                    cb.notify(errMsg, false);
                }
            }
        });
        mProtocolManager.submitWithUid(action);
    }
}
