package jp.concierge.jetkys.android;

import android.app.*;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;
import cn.jpush.android.api.JPushInterface;
import jp.concierge.jetkys.android.beans.LoginBean;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.gameutils.encrypt.EncryptData;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.manager.ManagerFactory;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.push.ChatManager;
import jp.concierge.jetkys.android.push.ZJPushReceiver;
import jp.concierge.jetkys.android.ui.*;
import jp.concierge.jetkys.android.utils.Utils;
import jp.concierge.jetkys.android.utils.ZLog;

import java.util.concurrent.ConcurrentHashMap;

public class MainActivity extends Activity {
    public static final int CONTAINER = R.id.realtabcontent;

    public static final String FILTER_LOGIN_COMPLETE = "MainActivity.FILTER_LOGIN_COMPLETE";

    public static final String TAB0 = "TopPageFragment";
    public static final String TAB1 = "FriendFragment";
    public static final String TAB2 = "MsgListFragment";
    public static final String TAB3 = "MailListFragment";
    public static final String TAB4 = "SearchFragment";
    public static final String TAB5 = "SettingFragment";

    public TabHost mTabHost;

    private TopPageFragment mTopPageFragment;
    private FriendFragment mFriendFragment;
    private MsgListFragment mMsgListFragment;
    private MailListFragment mMailListFragment;
    private SearchFragment mSearchFragment;
    private SettingFragment mSettingFragment;

    private int mCurrTab;

    public FragmentManager mFragmentManager;

    private Context mContext;

    private ConcurrentHashMap<Integer, BaseFragment.FResult> mResultMap;

    private LoginView mLoginView;
    private RegisterView mRegisterView;

    private NewMessageReceivedBroadCast mNewMessageReceivedBroadCast;
    private JpushReceivedBroadCast mJpushReceivedBroadCast;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    private void testTransfer(Fragment f) {
        getFragmentManager().beginTransaction().add(CONTAINER, f).commit();
    }

    public boolean checkEmpty(String param, String hint) {
        boolean con = TextUtils.isEmpty(param);
        if (con) {
            toast(String.format("%sを入力してください", hint));
        }
        return con;
    }

    private AccountManager mAccountManager;

    public ProgressDialog mLoadingDialog;

    private ChatManager mChatManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;

        mFragmentManager = getFragmentManager();

        mResultMap = new ConcurrentHashMap<>();

        initTab();

        mAccountManager = (AccountManager) ManagerFactory.getInstance().getManager(this, AccountManager.class);

        mLoginView = (LoginView) findViewById(R.id.loginview);
        mLoginView.setInit(this, mAccountManager);
        mRegisterView = (RegisterView) findViewById(R.id.registerview);
        mRegisterView.setInit(this, mAccountManager);

        mLoadingDialog = new ProgressDialog(this);
        mLoadingDialog.setCanceledOnTouchOutside(false);
        mLoadingDialog.setMessage("しばらくお待ちください");

        mChatManager = ChatManager.getInstance();
        mChatManager.init(this);

        mNewMessageReceivedBroadCast = new NewMessageReceivedBroadCast();
        mNewMessageReceivedBroadCast.register();

        mJpushReceivedBroadCast = new JpushReceivedBroadCast();
        mJpushReceivedBroadCast.register();

        try {
            Message msg = (Message) getIntent().getSerializableExtra("msg");
            tryStartChat(msg);
        } catch (Exception e) {
        }

        uploadPushToken();
    }

    private void tryStartChat(Message msg) {
        if (msg == null) return;

        if (TextUtils.isEmpty(mAccountManager.user_id)) return;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mTabHost.setCurrentTab(2);
            }
        }, 200);

//        Bundle bundle = new Bundle();
//        String whomKey = "1".equals(msg.vector) ? "cid" : "uid";
//        bundle.putString(whomKey, msg.from_id);
//        bundle.putString("title", msg.nickname);
//        bundle.putString("convid", msg.conversation_id);
//
//        final ChatFragment fragment = new ChatFragment();
//        fragment.setArguments(bundle);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                startFragment(fragment);
//            }
//        }, 200);
    }

    public void showLoading() {
        if (mLoadingDialog.isShowing()) return;

        mLoadingDialog.show();
    }

    public void dismissLoading() {
        mLoadingDialog.dismiss();
    }

    private void test() {
        String datas = "B53AA8015825F6CC75FEE710DCEBDA921088872081673A7868788C3C375070B90D0BAB357BFD1A663CF36A646D67A17778E946A1E3CD0E896962EAFC892A16C8829F362510B429E2E0E7F5B430FDB79195F00F59D175B2A38E6D638B9286A555368BB8DEF0139BDFAE8DFE2E08A05360E5F9A9D528C09CCE45F80FD642BC22D2A4763A51292D315C5F3634454A6E8A551582E8EAE531743338521DEB9AD6A9E0";
        String iv = "CE369B3ECA2285536BF179C6DDED154C";
        String realData = EncryptData.decrypt(datas, iv);

        ZLog.v("DECRYPT", realData);
    }

    @Override
    protected void onResume() {
        super.onResume();

        JPushInterface.onResume(mContext);

        if (!mAccountManager.isLogined()) {
            showLogin();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(mContext);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mLoginView.onDestory();
        mRegisterView.onDestory();

        mNewMessageReceivedBroadCast.unregister();
        mNewMessageReceivedBroadCast = null;

        mJpushReceivedBroadCast.unregister();
        mJpushReceivedBroadCast = null;

        mChatManager.mMainActivity = null;
    }

    private void initTab() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();
        mTabHost.setOnTabChangedListener(mTabChangeListener);
        mTabHost.setCurrentTab(0);

        TabHost.TabSpec spec0 = mTabHost.newTabSpec(TAB0);
        spec0.setIndicator(getTabIndicator(null, R.drawable.btn_tab_toppage));
        spec0.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec0);

        TabHost.TabSpec spec1 = mTabHost.newTabSpec(TAB1);
        spec1.setIndicator(getTabIndicator(null, R.drawable.btn_tab_friend));
        spec1.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec1);

        TabHost.TabSpec spec2 = mTabHost.newTabSpec(TAB2);
        spec2.setIndicator(getTabIndicator(null, R.drawable.btn_tab_msglist));
        spec2.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec2);

        TabHost.TabSpec spec3 = mTabHost.newTabSpec(TAB3);
        spec3.setIndicator(getTabIndicator(null, R.drawable.btn_tab_mail));
        spec3.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec3);

        TabHost.TabSpec spec4 = mTabHost.newTabSpec(TAB4);
        spec4.setIndicator(getTabIndicator(null, R.drawable.btn_tab_search));
        spec4.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec4);

        TabHost.TabSpec spec5 = mTabHost.newTabSpec(TAB5);
        spec5.setIndicator(getTabIndicator(null, R.drawable.btn_tab_setting));
        spec5.setContent(new DummyTabContent(mContext));
        mTabHost.addTab(spec5);
    }

    private View getTabIndicator(String name, int resId) {
        View v = getLayoutInflater().inflate(R.layout.item_tabhost, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_item_tabhost_name);
        tv.setText("" + name);
        ImageView iv = (ImageView) v.findViewById(R.id.iv_item_tabhost_icon);
        iv.setImageResource(resId);
        return v;
    }

    public void toMailList() {
        mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        mTabHost.setCurrentTab(3);
    }

    /**
     * 从Map里面取出Result
     *
     * @param fragmentHashCode
     * @return
     */
    public BaseFragment.FResult getResult(int fragmentHashCode) {
        BaseFragment.FResult result = mResultMap.get(fragmentHashCode);
        mResultMap.remove(fragmentHashCode);
        return result;
    }

    /**
     * 将新Fragment的result放进上个Fragment创建的Result中，等上个Fragment取
     *
     * @param hashKey
     * @param resultCode
     * @param datas
     */
    public void makeResult(int hashKey, int resultCode, Bundle datas) {
        BaseFragment.FResult result = mResultMap.get(hashKey);
        if (result == null) return;

        result.resultCode = resultCode;
        result.datas = datas;
    }

    public void startFragmentForResult(BaseFragment sourceF,
                                       BaseFragment targetF,
                                       int requestCode) {

        Bundle bundle = targetF.getArguments();
        if (bundle == null) bundle = new Bundle();

        bundle.putBoolean(BaseFragment.NEED_FRAGMENT_RESULT, true);
        int hasKey = sourceF.hashCode();
        bundle.putInt(BaseFragment.FRAGMENT_RESULT_KEY, hasKey);
        startFragment(targetF);

        BaseFragment.FResult fResult = new BaseFragment.FResult();
        fResult.requestCode = requestCode;
        mResultMap.put(hasKey, fResult);
    }

    public void startFragment(BaseFragment targetF) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.setCustomAnimations(
                android.R.animator.fade_in,
                android.R.animator.fade_out
        );
        ft.replace(MainActivity.CONTAINER, targetF);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void finishFragment() {
        mFragmentManager.popBackStack();
    }

    private void transTab0(FragmentTransaction transaction) {
        if (mTopPageFragment == null) {
            transaction.add(CONTAINER, new TopPageFragment(), TAB0);
        } else {
            transaction.attach(mTopPageFragment);
        }
    }

    private void transTab1(FragmentTransaction transaction) {
        if (mFriendFragment == null) {
            transaction.add(CONTAINER, new FriendFragment(), TAB1);
        } else {
            transaction.attach(mFriendFragment);
        }
    }

    private void transTab2(FragmentTransaction transaction) {
        if (mMsgListFragment == null) {
            transaction.add(CONTAINER, new MsgListFragment(), TAB2);
        } else {
            transaction.attach(mMsgListFragment);
        }
    }

    private void transTab3(FragmentTransaction transaction) {
        if (mMailListFragment == null) {
            transaction.add(CONTAINER, new MailListFragment(), TAB3);
        } else {
            transaction.attach(mMailListFragment);
        }
    }

    private void transTab4(FragmentTransaction transaction) {
        if (mSearchFragment == null) {
            transaction.add(CONTAINER, new SearchFragment(), TAB4);
        } else {
            transaction.attach(mSearchFragment);
        }
    }

    private void transTab5(FragmentTransaction transaction) {
        if (mSettingFragment == null) {
            transaction.add(CONTAINER, new SettingFragment(), TAB5);
        } else {
            transaction.attach(mSettingFragment);
        }
    }

    private TabHost.OnTabChangeListener mTabChangeListener = new TabHost.OnTabChangeListener() {

        @Override
        public void onTabChanged(String tabId) {
            mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            mTopPageFragment = (TopPageFragment) mFragmentManager.findFragmentByTag(TAB0);
            mFriendFragment = (FriendFragment) mFragmentManager.findFragmentByTag(TAB1);
            mMsgListFragment = (MsgListFragment) mFragmentManager.findFragmentByTag(TAB2);
            mMailListFragment = (MailListFragment) mFragmentManager.findFragmentByTag(TAB3);
            mSearchFragment = (SearchFragment) mFragmentManager.findFragmentByTag(TAB4);
            mSettingFragment = (SettingFragment) mFragmentManager.findFragmentByTag(TAB5);

            FragmentTransaction transaction = mFragmentManager.beginTransaction();

            if (mTopPageFragment != null) {
                transaction.detach(mTopPageFragment);
            }

            if (mFriendFragment != null) {
                transaction.detach(mFriendFragment);
            }

            if (mMsgListFragment != null) {
                transaction.detach(mMsgListFragment);
            }

            if (mMailListFragment != null) {
                transaction.detach(mMailListFragment);
            }

            if (mSearchFragment != null) {
                transaction.detach(mSearchFragment);
            }

            if (mSettingFragment != null) {
                transaction.detach(mSettingFragment);
            }

            if (tabId.equals(TAB0)) {
                transTab0(transaction);
            } else if (tabId.equals(TAB1)) {
                transTab1(transaction);
            } else if (tabId.equals(TAB2)) {
                transTab2(transaction);
            } else if (tabId.equals(TAB3)) {
                transTab3(transaction);
            } else if (tabId.equals(TAB4)) {
                transTab4(transaction);
            } else if (tabId.equals(TAB5)) {
                transTab5(transaction);
            }
            transaction.commitAllowingStateLoss();
            mCurrTab = mTabHost.getCurrentTab();
        }
    };

    public static class DummyTabContent implements TabHost.TabContentFactory {
        private Context mContext;

        public DummyTabContent(Context context) {
            mContext = context;
        }

        @Override
        public View createTabContent(String s) {
            View v = new View(mContext);
            return v;
        }
    }

    public void toast(String msg) {
        Utils.toast(mContext, msg);
    }

    private class NewMessageReceivedBroadCast extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            registerReceiver(this, new IntentFilter(ChatManager.FILTER_NEW_MSG_RECEIVED));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                return;
            }

            final Message msg = (Message) intent.getSerializableExtra("msg");

            if (!mChatManager.conversationId.equals(msg.conversation_id)
                    && mChatManager.mChatFragment != null) {
                // 如果当前聊天的人不是这个人，则消失
                mChatManager.mChatFragment.finish();
            }

            tryStartChat(msg);
        }
    }

    private class JpushReceivedBroadCast extends BroadcastReceiver {

        public void unregister() {
            unregisterReceiver(this);
        }

        public void register() {
            registerReceiver(this, new IntentFilter(ZJPushReceiver.FILTER_JPUSH_RECEIVED));
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            uploadPushToken();
        }
    }

    private long exitTime = 0l;

    @Override
    public void onBackPressed() {
        if (mRegisterView.getVisibility() == View.VISIBLE) {
            showLogin();
        } else {
            if (mFragmentManager.getBackStackEntryCount() > 0 ||
                    Math.abs(exitTime - System.currentTimeMillis()) < 2000) {
                super.onBackPressed();
            } else {
                toast("もう一回バックキー押すとアプリ終了します");
                exitTime = System.currentTimeMillis();
            }
        }
    }

    public void moveToRegister() {
        AlphaAnimation alpha = new AlphaAnimation(0.0f, 1.0f);
        alpha.setDuration(400);
        mRegisterView.startAnimation(alpha);
        mRegisterView.setVisibility(View.VISIBLE);
        mRegisterView.setInit(this, mAccountManager);
    }

    public void showLogin() {
        int count = mTabHost.getTabWidget().getChildCount();
        for (int i = 0; i < count; i++) {
            mTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(false);
        }

        mLoginView.setVisibility(View.VISIBLE);
        mRegisterView.setVisibility(View.GONE);
    }

    public void dismissLogin() {
        mLoginView.setVisibility(View.GONE);
        mRegisterView.setVisibility(View.GONE);

        Utils.disableKeyBoard(this);
    }

    public void onLoginComplete() {
        int count = mTabHost.getTabWidget().getChildCount();
        for (int i = 0; i < count; i++) {
            mTabHost.getTabWidget().getChildTabViewAt(i).setEnabled(true);
        }

        mTabHost.setEnabled(true);
        sendBroadcast(new Intent(FILTER_LOGIN_COMPLETE));
        uploadPushToken();
    }

    private void uploadPushToken() {
        String uid = mAccountManager.user_id;
        String token = SharedPreferenceHelper.getString("jpush_regid");

        if (TextUtils.isEmpty(uid) || TextUtils.isEmpty(token)) return;

        mAccountManager.uploadPushToken(token, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
    }

    public void relogin() {
        mAccountManager.clearLoginstatus();

        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
