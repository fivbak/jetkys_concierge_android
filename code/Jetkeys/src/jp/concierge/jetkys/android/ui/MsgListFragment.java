package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.Conversation;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.PullListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/21.
 */
public class MsgListFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_msglist;
    }

    @Bind(R.id.pulllistview)
    PullListView mPullListView;

    private ListAdapter mListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mListAdapter = new ListAdapter();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        mPullListView.setAdapter(mListAdapter);
        mPullListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mPullListView.getRefreshableView().getHeaderViewsCount();
                Conversation item = mListAdapter.getItem(pos);

                Bundle bundle = new Bundle();
                bundle.putString("convid", item.id);
                bundle.putString("cid", item.concierge_id);
                bundle.putString("did", item.diagnosis_id);
                bundle.putString("uid", item.user_id);
                bundle.putString("title", item.nickname);

                ChatFragment fragment = new ChatFragment();
                fragment.setArguments(bundle);

                startFragment(fragment);
            }
        });
        mPullListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                refresh();
            }
        });

        if (savedBundle != null) {
            int first = savedBundle.getInt("first", 0);
            mPullListView.getRefreshableView().setSelection(first);
        }
    }

    @Override
    protected void saveBundle(Bundle savedBundle) {
        super.saveBundle(savedBundle);

        int first = mPullListView.getRefreshableView().getFirstVisiblePosition();
        savedBundle.putInt("first", first);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPullListView.onRefreshComplete();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refresh();
    }

    private void refresh() {
        mNetManager.getConversationList(new Callback<List<Conversation>>() {
            @Override
            protected void onSucceed(List<Conversation> result) {
                mPullListView.onRefreshComplete();

                mListAdapter.clear();
                mListAdapter.addAll(result);
            }

            @Override
            protected void onError(Object object) {
                mPullListView.onRefreshComplete();
            }
        });
    }

    private class ListAdapter extends ArrayAdapter<Conversation> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_message, null);

                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_avatar);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_name);
                h.desc = (TextView) convertView.findViewById(R.id.tv_item_list_desc);
                h.sign = convertView.findViewById(R.id.iv_msg_sign);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            Conversation item = getItem(position);
            h.sign.setVisibility("1".equals(item.if_msg) ? View.VISIBLE : View.INVISIBLE);
            asyncLoadImage(item.image, h.avatar);
            h.name.setText(item.nickname);
            h.desc.setText(item.message);

            return convertView;
        }

        private class Holder {
            ImageView avatar;
            TextView name;
            TextView desc;
            View sign;
        }
    }

}
