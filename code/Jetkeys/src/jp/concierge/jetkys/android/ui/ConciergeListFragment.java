package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.base.BaseConciergeListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.PullListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/19.
 */
public class ConciergeListFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_concierge;
    }

    @Bind(R.id.pulllistview)
    PullListView mListView;

    private ListAdapter mAdapter;

    private String categories, type, gender;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ListAdapter();

        Bundle bundle = getArguments();
        if (bundle == null) {
            categories = "";
            type = "";
            gender = "";
        } else {
            categories = bundle.getString("cat", "");
            type = bundle.getString("type", "0");
            gender = bundle.getString("gender", "");
        }
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListView.getRefreshableView().getHeaderViewsCount();
                Concierge item = mAdapter.getItem(pos);
                Bundle bundle = new Bundle();
                bundle.putString("id", item.id);
                bundle.putString("name", item.getName());

                MemberFragment fragment = new MemberFragment();
                fragment.setArguments(bundle);
                startFragment(fragment);
            }
        });
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                refresh();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                loadmore();
            }
        });

        if (savedBundle != null) {
            int first = savedBundle.getInt("first", 0);
            mListView.getRefreshableView().setSelection(first);
        }
    }

    @Override
    protected void saveBundle(Bundle savedBundle) {
        super.saveBundle(savedBundle);

        int first = mListView.getRefreshableView().getFirstVisiblePosition();
        savedBundle.putInt("first", first);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListView.onRefreshComplete();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refresh();
    }

    private void test() {
        List<Concierge> concierges = new ArrayList<Concierge>();
        for (int i = 0; i < 10; i++) {
            Concierge concierge = new Concierge();
            concierge.self_introduction = "DESCDESCDESC" + i;
            concierge.lname = "NICKNAME" + i;
            concierges.add(concierge);
        }
        mAdapter.addAll(concierges);
    }

    private void refresh() {
        mNetManager.getConciergeList(
                categories, type, gender,
                NetManager.PAGE_FIRST, NetManager.PAGE_NUM,
                mAccountManager.concierge_id,
                new Callback<BaseConciergeListEntity>() {
                    @Override
                    protected void onSucceed(BaseConciergeListEntity result) {
                        mListView.onRefreshComplete();
                        mAdapter.clear();

                        List<Concierge> list = result.concierges;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void loadmore() {
        int end = mAdapter.getCount() + 1;
        mNetManager.getConciergeList(
                categories, type, gender,
                end, end + NetManager.PAGE_NUM,
                mAccountManager.concierge_id,
                new Callback<BaseConciergeListEntity>() {
                    @Override
                    protected void onSucceed(BaseConciergeListEntity result) {
                        mListView.onRefreshComplete();

                        List<Concierge> list = result.concierges;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private class ListAdapter extends ArrayAdapter<Concierge> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_concierge, null);

                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_avatar);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_name);
                h.desc = (TextView) convertView.findViewById(R.id.tv_item_list_desc);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            Concierge concierge = getItem(position);
            h.name.setText(concierge.getName());
            h.desc.setText(concierge.self_introduction);

            return convertView;
        }

        private class Holder {
            ImageView avatar;
            TextView name;
            TextView desc;
        }
    }
}
