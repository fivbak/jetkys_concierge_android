package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.base.BaseMailListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.PullListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/19.
 */
public class MailListInnerFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_maillist_inner;
    }

    //    public static final int COUNT = 14;
    @Bind(R.id.pulllistview)
    PullListView mListView;

    private ListAdapter mAdapter;

    private String uid, cid;
    private String name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        uid = bundle.getString("uid");
        cid = bundle.getString("cid");
        name = bundle.getString("name");

        mAdapter = new ListAdapter();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        setTitle(name);
        needBackFinish();

        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListView.getRefreshableView().getHeaderViewsCount();
                MailBean item = mAdapter.getItem(pos);

                Bundle bundle = new Bundle();
                bundle.putString("id", item.id);
                MailDetailFragment fragment = new MailDetailFragment();
                fragment.setArguments(bundle);
                startFragment(fragment);
            }
        });
        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                refresh();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                loadmore();
            }
        });

        if (savedBundle != null) {
            int first = savedBundle.getInt("first", 0);
            mListView.getRefreshableView().setSelection(first);
        }
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refresh();
    }

    @Override
    protected void saveBundle(Bundle savedBundle) {
        super.saveBundle(savedBundle);

        int first = mListView.getRefreshableView().getFirstVisiblePosition();
        savedBundle.putInt("first", first);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListView.onRefreshComplete();
    }

    private void refresh() {
        mNetManager.getMailFriendList(uid, cid, NetManager.PAGE_FIRST,
                new Callback<BaseMailListEntity>() {
                    @Override
                    protected void onSucceed(BaseMailListEntity result) {
                        mListView.onRefreshComplete();
                        mAdapter.clear();

                        List<MailBean> list = result.items;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void loadmore() {
        int end = mAdapter.getCount() + 1;
        mNetManager.getMailFriendList(uid, cid, end,
                new Callback<BaseMailListEntity>() {
                    @Override
                    protected void onSucceed(BaseMailListEntity result) {
                        mListView.onRefreshComplete();

                        List<MailBean> list = result.items;
                        mAdapter.addAll(list);
                    }

                    @Override
                    protected void onError(Object object) {
                        mListView.onRefreshComplete();
                    }
                });
    }

    private void test() {
        List<MailBean> members = new ArrayList<MailBean>();
        for (int i = 0; i < 10; i++) {
            MailBean member = new MailBean();
            members.add(member);
        }
        mAdapter.addAll(members);
    }

    private class ListAdapter extends ArrayAdapter<MailBean> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_maillist, null);

                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_avatar);
                h.status = (ImageView) convertView.findViewById(R.id.iv_item_list_status);
                h.attachment = (ImageView) convertView.findViewById(R.id.iv_item_list_file);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_name);
                h.desc = (TextView) convertView.findViewById(R.id.tv_item_list_desc);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            MailBean item = getItem(position);
            asyncLoadImage(item.image_url, h.avatar);
            h.name.setText(item.nickname);
            h.desc.setText(item.title);

            boolean read = "1".equals(item.status);
            h.status.setImageResource(read ? R.drawable.icon_mail_open : R.drawable.icon_mail_closed);

            boolean hasAttachment = "1".equals(item.attachment);
            h.attachment.setVisibility(hasAttachment ? View.VISIBLE : View.INVISIBLE);

            return convertView;
        }

        private class Holder {
            ImageView avatar;
            ImageView status;
            ImageView attachment;
            TextView name;
            TextView desc;
        }
    }
}
