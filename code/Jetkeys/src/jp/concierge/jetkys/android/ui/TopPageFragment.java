package jp.concierge.jetkys.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.MainActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.SysMessage;
import jp.concierge.jetkys.android.beans.base.BaseConciergeListEntity;
import jp.concierge.jetkys.android.beans.base.BaseNoticeListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.TopPagePeopleView;

import java.util.List;

/**
 * Created by WilliZ on 16/03/21.
 */
public class TopPageFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_toppage;
    }

    @Bind({
            R.id.tv_msg0, R.id.tv_msg1, R.id.tv_msg2, R.id.tv_msg3,
    })
    TextView[] mSysMsgTitleTvs;

    @Bind({
            R.id.tv_time0, R.id.tv_time1, R.id.tv_time2, R.id.tv_time3,
    })
    TextView[] mSysMsgTimeTvs;

    @Bind({
            R.id.concv_toppage0, R.id.concv_toppage1, R.id.concv_toppage2, R.id.concv_toppage3,
    })
    TopPagePeopleView[] mConcViews;

    @OnClick(R.id.layout_top_more)
    void moreClick() {
        ConciergeListFragment fragment = new ConciergeListFragment();
        startFragment(fragment);
    }

    @OnClick(R.id.layout_msg0)
    void msg0Click() {
        startFragment(new SysmsglistFragment());
    }

    @OnClick(R.id.layout_msg1)
    void msg1Click() {
        startFragment(new SysmsglistFragment());
    }

    @OnClick(R.id.layout_msg2)
    void msg2Click() {
        startFragment(new SysmsglistFragment());
    }

    @OnClick(R.id.layout_msg3)
    void msg3Click() {
        startFragment(new SysmsglistFragment());
    }

    @OnClick(R.id.btn_top_btn0)
    void btn0Click() {
        toConList("14");
    }

    @OnClick(R.id.btn_top_btn1)
    void btn1Click() {
        toConList("3");
    }

    @OnClick(R.id.btn_top_btn2)
    void btn2Click() {
        toConList("10");
    }

    @OnClick(R.id.btn_top_btn3)
    void btn3Click() {
        toConList("7");
    }

    private void toConList(String category) {
        Bundle bundle = new Bundle();
        bundle.putString("cat", category);

        ConciergeListFragment fragment = new ConciergeListFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    private List<SysMessage> mSysMsgList;
    private List<Concierge> mConciergeList;

    private LoginReceiver mLoginReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLoginReceiver = new LoginReceiver();
        mLoginReceiver.register();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mLoginReceiver.unregister();
        mLoginReceiver = null;
    }

    @Override
    protected void initViews(Bundle bundle) {
        super.initViews(bundle);

        for (int i = 0; i < mConcViews.length; i++) {
            final int pos = i;
            mConcViews[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Concierge conc = mConciergeList.get(pos);
                        Bundle b = new Bundle();
                        b.putString("id", conc.id);
                        b.putString("name", conc.getName());

                        MemberFragment fragment = new MemberFragment();
                        fragment.setArguments(b);
                        startFragment(fragment);
                    } catch (Exception e) {
                    }
                }
            });
        }

        setMessages();
        setConcierges();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        if (!mAccountManager.isLogined()) return;

        mNetManager.getConciergeList("", "", "",
                NetManager.PAGE_FIRST, NetManager.PAGE_FIRST + 3,
                "",
                new Callback<BaseConciergeListEntity>() {
                    @Override
                    protected void onSucceed(BaseConciergeListEntity result) {
                        dismissLoading();
                        mConciergeList = result.concierges;
                        setConcierges();
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();

        mNetManager.getNoticeList(NetManager.PAGE_FIRST, NetManager.PAGE_FIRST + 3,
                new Callback<BaseNoticeListEntity>() {
                    @Override
                    protected void onSucceed(BaseNoticeListEntity result) {
                        mSysMsgList = result.notice_list;

                        setMessages();
                    }

                    @Override
                    protected void onError(Object object) {
                    }
                });
    }

    private void setMessages() {
        if (mSysMsgList == null) return;

        try {
            for (int i = 0; i < 4; i++) {
                SysMessage msg = mSysMsgList.get(i);
                mSysMsgTimeTvs[i].setText(msg.date);
                mSysMsgTitleTvs[i].setText(msg.title);
            }
        } catch (Exception e) {
        }
    }

    private void setConcierges() {
        if (mConciergeList == null) return;

        for (int i = 0; i < 4; i++) {
            try {
                Concierge conc = mConciergeList.get(i);
                mConcViews[i].setConc(conc, this);
            } catch (Exception e) {
                mConcViews[i].setVisibility(View.GONE);
            }
        }
    }

    private class LoginReceiver extends BroadcastReceiver {

        public void register() {
            mActivity.registerReceiver(this, new IntentFilter(MainActivity.FILTER_LOGIN_COMPLETE));
        }

        public void unregister() {
            mActivity.unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            onFirstTimeInit();
        }
    }
}