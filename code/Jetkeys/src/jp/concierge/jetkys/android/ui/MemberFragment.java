package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.Concierge;
import jp.concierge.jetkys.android.beans.Diagnosis;
import jp.concierge.jetkys.android.beans.base.BaseDiagnosisListEntity;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.InnerScrollListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/21.
 */
public class MemberFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_member;
    }

    @Bind(R.id.btn_send)
    View mSubmitBtn;

    @OnClick(R.id.btn_send)
    void sendClick() {
        Bundle bundle = new Bundle();
        bundle.putString("cid", mConcierge.id);
        bundle.putString("title", mConcierge.getName());

        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    @Bind(R.id.layout_scroll_root)
    ScrollView mScrollView;

    @Bind(R.id.tv_member_job)
    TextView mJobsTv;

    @Bind(R.id.iv_member_avatar)
    ImageView mAvatarIv;

    @Bind(R.id.tv_member_name)
    TextView mNickTv;
    @Bind(R.id.tv_member_character)
    TextView mCharTv;
    @Bind(R.id.tv_member_spec)
    TextView mSpecTv;
    @Bind(R.id.tv_member_desc)
    TextView mDescTv;

    @Bind(R.id.lv_setting_ident)
    InnerScrollListView mListView;

    private ListAdapter mAdapter;

    private String cid;
    private String cname;
    private boolean mIsPreview;

    private Concierge mConcierge;
    private String limitHour;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ListAdapter();

        Bundle bundle = getArguments();
        cid = bundle.getString("id");
        cname = bundle.getString("name");
        mIsPreview = bundle.getBoolean("preview", false);

        if (mAccountManager.concierge_id.equals(cid)) {
            mIsPreview = true;
        }
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        setTitle(cname);
        needBackFinish();

        mListView.setAdapter(mAdapter);

        if (mIsPreview) {
            mSubmitBtn.setVisibility(View.GONE);
            mListView.setOnItemClickListener(null);
        } else {
            mSubmitBtn.setVisibility(View.VISIBLE);
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Diagnosis diagnosis = mAdapter.getItem(position);
                    Bundle bundle = new Bundle();
                    bundle.putString("id", diagnosis.id);
                    bundle.putString("limit", limitHour);

                    DiagnosisFragment fragment = new DiagnosisFragment();
                    fragment.setArguments(bundle);
                    startFragment(fragment);
                }
            });
        }

        setDatas();

        if (savedBundle == null) {
            mScrollView.smoothScrollTo(0, 0);
        } else {
            int scrollx = savedBundle.getInt("scrollx");
            int scrolly = savedBundle.getInt("scrolly");

            mScrollView.scrollTo(scrollx, scrolly);
        }
    }

    @Override
    protected void saveBundle(Bundle savedBundle) {
        super.saveBundle(savedBundle);
        savedBundle.putInt("scrollx", mScrollView.getScrollX());
        savedBundle.putInt("scrolly", mScrollView.getScrollY());
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        mNetManager.getConciergeDetail(cid, new Callback<Concierge>() {
            @Override
            protected void onSucceed(Concierge result) {
                dismissLoading();
                mConcierge = result;
                setDatas();
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });

        showLoading();

        mNetManager.getDiagnosisList(cid, new Callback<BaseDiagnosisListEntity>() {
            @Override
            protected void onSucceed(BaseDiagnosisListEntity result) {
                limitHour = result.limit_hour;
                List<Diagnosis> list = result.diagnoses;
                mAdapter.addAll(list);
            }

            @Override
            protected void onError(Object object) {
            }
        });
    }

    private void test() {
        List<Diagnosis> list = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Diagnosis bean = new Diagnosis();
            bean.message = "DESCDESCDESC" + i;
            bean.point = (1000 + i) + "";
            list.add(bean);
        }
        mAdapter.addAll(list);
    }

    private void setDatas() {
        if (mConcierge == null) return;

        asyncLoadImage(mConcierge.image, mAvatarIv);
        mNickTv.setText(mConcierge.getName());
        String age = mConcierge.age + "歳";
        String gender = "1".equals(mConcierge.gender) ? "男" : "女";
        mCharTv.setText(age + "/" + gender);
        try {
            String[] items = mConcierge.capacity.split("rn");
            String sb = "";
            for (int i = 0; i < items.length; i++) {
                sb += items[i];
                if (i < items.length - 1) {
                    sb += "\n";
                }
            }
            mJobsTv.setText(sb);
        } catch (Exception e) {
            mJobsTv.setText(mConcierge.capacity);
        }
        mDescTv.setText(mConcierge.self_introduction);

        setCategories();
    }

    private void setCategories() {
        List<String> list = mConcierge.concierges_categories;
        StringBuilder sb = new StringBuilder();
        int len = list.size();
        for (int i = 0; i < len; i++) {
            sb.append("· " + list.get(i));
            if (i != len - 1) {
                sb.append("\n");
            }
        }
        mSpecTv.setText(sb.toString());
    }

    private class ListAdapter extends ArrayAdapter<Diagnosis> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_ident, null);

                h.name = (TextView) convertView.findViewById(R.id.tv_ident_name);
                h.price = (TextView) convertView.findViewById(R.id.tv_ident_price);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            Diagnosis item = getItem(position);
            h.name.setText(item.title);
            h.price.setText(item.point + "円");

            return convertView;
        }

        private class Holder {
            TextView name;
            TextView price;
        }
    }
}