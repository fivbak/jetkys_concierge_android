package jp.concierge.jetkys.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.Settings;
import jp.concierge.jetkys.android.beans.Message;
import jp.concierge.jetkys.android.net.thread.Callback;

/**
 * Created by WilliZ on 16/03/20.
 */
public class SettingSwithFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_setting_switch;
    }

    @Bind(R.id.tbutton_setting_status_icon)
    ImageView mStatusIcon;

    @OnClick(R.id.tbutton_setting_status_icon)
    void settingStatusIcon() {
        final boolean on = Settings.get(Settings.STATUS_ICON);

        setSwitch(mStatusIcon, !on);
        Settings.save(Settings.STATUS_ICON, !on);
    }

    @Bind(R.id.tbutton_setting_sound)
    ImageView mSound;

    @OnClick(R.id.tbutton_setting_sound)
    void settingSound() {
        final boolean on = Settings.get(Settings.SOUND);

        setSwitch(mSound, !on);
        Settings.save(Settings.SOUND, !on);
    }

    @Bind(R.id.tbutton_setting_vibration)
    ImageView mVibration;

    @OnClick(R.id.tbutton_setting_vibration)
    void settingVibration() {
        final boolean on = Settings.get(Settings.VIBRATION);

        setSwitch(mVibration, !on);
        Settings.save(Settings.VIBRATION, !on);
    }

    @Bind(R.id.tbutton_setting_msg_notif)
    ImageView mMsgNotif;

    @OnClick(R.id.tbutton_setting_msg_notif)
    void settingMsgNotif() {
        final boolean on = Settings.get(Settings.MESSAGE_NOTIF);

        setSwitch(mMsgNotif, !on);
        Settings.save(Settings.MESSAGE_NOTIF, !on);
    }

    @Bind(R.id.tbutton_setting_pay_confirm_send_mail)
    ImageView mPayConfirmSendMail;

    @OnClick(R.id.tbutton_setting_pay_confirm_send_mail)
    void settingPayConfirmSendMail() {
        final boolean on = Settings.get(Settings.PAY_CONFIRM_SEND_MAIL);

        submitSetting("setting_mail", !on);
    }

    @Bind(R.id.tbutton_setting_pay_confirm_send_msg)
    ImageView mPayConfirmSendMsg;

    @OnClick(R.id.tbutton_setting_pay_confirm_send_msg)
    void settingPayConfirmSendMsg() {
        final boolean on = Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE);

        submitSetting("setting_message", !on);
    }

    @Bind(R.id.tbutton_setting_pay_confirm_picture)
    ImageView mPayConfirmPicture;

    @OnClick(R.id.tbutton_setting_pay_confirm_picture)
    void settingPayConfirmPicutre() {
        final boolean on = Settings.get(Settings.PAY_CONFIRM_SEE_PICTURE);

        submitSetting("setting_image", !on);
    }

    public static final String MAIL_ADDRESS = "manage@jetkys.com";
    public static final String MAIL_SUBJECT = "subject";
    public static final String MAIL_TEXT = "text";

    @OnClick(R.id.layout_setting_switch_delete)
    void deleteAccount() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{MAIL_ADDRESS});
        intent.putExtra(Intent.EXTRA_SUBJECT, MAIL_SUBJECT);
        intent.putExtra(Intent.EXTRA_TEXT, MAIL_TEXT);
        intent.setType("text/plain");
        startActivity(intent);
//        mActivity.relogin();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setSwitch(mStatusIcon, Settings.get(Settings.STATUS_ICON));
        setSwitch(mSound, Settings.get(Settings.SOUND));
        setSwitch(mVibration, Settings.get(Settings.VIBRATION));
        setSwitch(mMsgNotif, Settings.get(Settings.MESSAGE_NOTIF));
        setSwitch(mPayConfirmSendMail, Settings.get(Settings.PAY_CONFIRM_SEND_MAIL));
        setSwitch(mPayConfirmSendMsg, Settings.get(Settings.PAY_CONFIRM_SEND_MESSAGE));
        setSwitch(mPayConfirmPicture, Settings.get(Settings.PAY_CONFIRM_SEE_PICTURE));
    }

    private void setSwitch(ImageView v, boolean on) {
        v.setImageResource(on ? R.drawable.toggle_setting_on : R.drawable.toggle_setting_off);
    }

    private void submitSetting(final String key, final boolean value) {
        mNetManager.submitSetting(key, value, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();
                if ("setting_mail".equals(key)) {
                    setSwitch(mPayConfirmSendMail, value);
                    Settings.save(Settings.PAY_CONFIRM_SEND_MAIL, value);
                } else if ("setting_message".equals(key)) {
                    setSwitch(mPayConfirmSendMsg, value);
                    Settings.save(Settings.PAY_CONFIRM_SEND_MESSAGE, value);
                } else if ("setting_image".equals(key)) {
                    setSwitch(mPayConfirmPicture, value);
                    Settings.save(Settings.PAY_CONFIRM_SEE_PICTURE, value);
                }
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }
}
