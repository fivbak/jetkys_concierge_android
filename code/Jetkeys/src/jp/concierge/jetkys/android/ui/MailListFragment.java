package jp.concierge.jetkys.android.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import butterknife.BindColor;
import butterknife.OnClick;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.base.BaseMailListEntity;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.views.PullListView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WilliZ on 16/03/19.
 */
public class MailListFragment extends BaseFragment {
    public static final String FILTER_MAIL_NEED_REFRESH = "MailListFragment.FILTER_MAIL_NEED_REFRESH";

    @Override
    protected int layout() {
        return R.layout.fragment_maillist;
    }

    private String mCurrType = "0";

    @BindColor(R.color.pink)
    int PINK;

    @Bind(R.id.btn_tab0)
    Button mTab0;

    @OnClick(R.id.btn_tab0)
    void tab0Click() {
        if (mRequesting) {
            toast("処理中です。しばらく待ってから再度お試してください。");
            return;
        }

        if ("0".equals(mCurrType)) return;

        refreshInbox();
    }

    @Bind(R.id.btn_tab1)
    Button mTab1;

    @OnClick(R.id.btn_tab1)
    void tab1Click() {
        if (mRequesting) {
            toast("処理中です。しばらく待ってから再度お試してください。");
            return;
        }

        if ("1".equals(mCurrType)) return;

        refreshSend();
    }

    private void setType(String type) {
        mCurrType = type;

        if ("0".equals(type)) {
            mTab0.setTextColor(PINK);
            mTab0.setBackgroundColor(Color.WHITE);
            mTab1.setTextColor(Color.WHITE);
            mTab1.setBackgroundColor(PINK);
        } else if ("1".equals(type)) {
            mTab1.setTextColor(PINK);
            mTab1.setBackgroundColor(Color.WHITE);
            mTab0.setTextColor(Color.WHITE);
            mTab0.setBackgroundColor(PINK);
        }
    }

    //    public static final int COUNT = 14;
    @Bind(R.id.pulllistview)
    PullListView mListView;

    private ListAdapter mAdapter;

    private boolean mRequesting = false;

    private MailRefreshReceiver mMailRefreshReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ListAdapter();

        mMailRefreshReceiver = new MailRefreshReceiver();
        mMailRefreshReceiver.register();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mMailRefreshReceiver.unregister();
        mMailRefreshReceiver = null;
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int pos = position - mListView.getRefreshableView().getHeaderViewsCount();
                MailBean item = mAdapter.getItem(pos);
                item.status = "1";

                Bundle bundle = new Bundle();
                bundle.putString("id", item.id);
                bundle.putString("name", item.nickname);
                bundle.putString("cid", item.concierge_id);
                MailListInnerFragment fragment = new MailListInnerFragment();
                fragment.setArguments(bundle);
                startFragment(fragment);
            }
        });

        mListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                if ("0".equals(mCurrType)) {
                    refreshInbox();
                } else if ("1".equals(mCurrType)) {
                    refreshSend();
                }
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if ("0".equals(mCurrType)) {
                    loadMoreInbox();
                } else if ("1".equals(mCurrType)) {
                    loadMoreSend();
                }
            }
        });

        if (savedBundle != null) {
            int first = savedBundle.getInt("first", 0);
            mListView.getRefreshableView().setSelection(first);

            String type = savedBundle.getString("type");
            setType(type);
        }
    }

    @Override
    protected void saveBundle(Bundle savedBundle) {
        super.saveBundle(savedBundle);

        int first = mListView.getRefreshableView().getFirstVisiblePosition();
        savedBundle.putInt("first", first);
        savedBundle.putString("type", mCurrType);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListView.onRefreshComplete();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refreshInbox();
    }

    private void test() {
        List<MailBean> list = new ArrayList<MailBean>();
        for (int i = 0; i < 10; i++) {
            MailBean item = new MailBean();
            list.add(item);
        }
        mAdapter.addAll(list);
    }

    private void refreshInbox() {
        mNetManager.getMailInboxList(NetManager.PAGE_FIRST, new Callback<BaseMailListEntity>() {
            @Override
            protected void onSucceed(BaseMailListEntity result) {
                mListView.onRefreshComplete();
                mRequesting = false;

                setType("0");

                mAdapter.clear();

                List<MailBean> list = result.items;
                mAdapter.addAll(list);
            }

            @Override
            protected void onError(Object object) {
                mListView.onRefreshComplete();
                mRequesting = false;
            }
        });
        mRequesting = true;
    }

    private void loadMoreInbox() {
        int last = mAdapter.getCount() + 1;
        mNetManager.getMailInboxList(last, new Callback<BaseMailListEntity>() {
            @Override
            protected void onSucceed(BaseMailListEntity result) {
                mListView.onRefreshComplete();
                mRequesting = false;

                List<MailBean> list = result.items;
                mAdapter.addAll(list);
            }

            @Override
            protected void onError(Object object) {
                mListView.onRefreshComplete();
                mRequesting = false;
            }
        });
        mRequesting = true;
    }

    private void refreshSend() {
        mNetManager.getMailSendList(NetManager.PAGE_FIRST, new Callback<BaseMailListEntity>() {
            @Override
            protected void onSucceed(BaseMailListEntity result) {
                mListView.onRefreshComplete();
                mRequesting = false;

                setType("1");

                mAdapter.clear();

                List<MailBean> list = result.items;
                mAdapter.addAll(list);
            }

            @Override
            protected void onError(Object object) {
                mListView.onRefreshComplete();
                mRequesting = false;
            }
        });
        mRequesting = true;
    }

    private void loadMoreSend() {
        int last = mAdapter.getCount() + 1;
        mNetManager.getMailInboxList(last, new Callback<BaseMailListEntity>() {
            @Override
            protected void onSucceed(BaseMailListEntity result) {
                mListView.onRefreshComplete();
                mRequesting = false;

                List<MailBean> list = result.items;
                mAdapter.addAll(list);
            }

            @Override
            protected void onError(Object object) {
                mListView.onRefreshComplete();
                mRequesting = false;
            }
        });
        mRequesting = true;
    }

    private class MailRefreshReceiver extends BroadcastReceiver {

        public void register() {
            mActivity.registerReceiver(this, new IntentFilter(FILTER_MAIL_NEED_REFRESH));
        }

        public void unregister() {
            mActivity.unregisterReceiver(this);
        }

        @Override
        public void onReceive(Context context, Intent intent) {
//            if ("0".equals(mCurrType)) {
//                refreshInbox();
//            } else if ("1".equals(mCurrType)) {
                refreshSend(); // 要求跳转到送信box
//            }
        }
    }

    private class ListAdapter extends ArrayAdapter<MailBean> {
        private LayoutInflater mInflater;

        public ListAdapter() {
            super(mActivity, -1);

            mInflater = mActivity.getLayoutInflater();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder h = null;
            if (convertView == null) {
                h = new Holder();
                convertView = mInflater.inflate(R.layout.item_list_maillist, null);

                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_avatar);
                h.status = (ImageView) convertView.findViewById(R.id.iv_item_list_status);
                h.attachment = (ImageView) convertView.findViewById(R.id.iv_item_list_file);
                h.name = (TextView) convertView.findViewById(R.id.tv_item_list_name);
                h.desc = (TextView) convertView.findViewById(R.id.tv_item_list_desc);

                convertView.setTag(h);
            } else {
                h = (Holder) convertView.getTag();
            }

            MailBean item = getItem(position);
            asyncLoadImage(item.profile_image_url, h.avatar);
            h.name.setText(item.nickname);
            h.desc.setText(item.title);

            boolean read = "1".equals(item.status);
            h.status.setImageResource(read ? R.drawable.icon_mail_open : R.drawable.icon_mail_closed);

            boolean hasAttachment = "1".equals(item.attachment);
            h.attachment.setVisibility(hasAttachment ? View.VISIBLE : View.INVISIBLE);

            return convertView;
        }

        private class Holder {
            ImageView avatar;
            ImageView status;
            ImageView attachment;
            TextView name;
            TextView desc;
        }
    }
}
