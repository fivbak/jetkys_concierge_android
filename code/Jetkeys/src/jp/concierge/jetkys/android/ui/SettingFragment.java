package jp.concierge.jetkys.android.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.BindColor;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.beans.UserInfo;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;

/**
 * Created by WilliZ on 16/03/19.
 */
public class SettingFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_setting;
    }

    private String mCurrType = "1";

    @BindColor(R.color.pink)
    int PINK;

    @Bind(R.id.btn_tab0)
    Button mTab0;

    @OnClick(R.id.btn_tab0)
    void tab0Click() {
        if (mRequesting) return;

        if ("1".equals(mCurrType)) return;

        refresh("1");
    }

    @Bind(R.id.btn_tab1)
    Button mTab1;

    @OnClick(R.id.btn_tab1)
    void tab1Click() {
        if (mRequesting) return;

        if ("2".equals(mCurrType)) return;

        refresh("2");
    }

    private void setType(String type) {
        mCurrType = type;

        if ("1".equals(type)) {
            mTab0.setTextColor(PINK);
            mTab0.setBackgroundColor(Color.WHITE);
            mTab1.setTextColor(Color.WHITE);
            mTab1.setBackgroundColor(PINK);
        } else if ("2".equals(type)) {
            mTab1.setTextColor(PINK);
            mTab1.setBackgroundColor(Color.WHITE);
            mTab0.setTextColor(Color.WHITE);
            mTab0.setBackgroundColor(PINK);
        }
    }

    private boolean mRequesting = false;

    @OnClick(R.id.btn_buy)
    void buyPoint() {
        Bundle bundle = new Bundle();
        bundle.putString("point", mPointTv.getText().toString());

        BuyPointFragment fragment = new BuyPointFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    @OnClick(R.id.layout_setting_sysmsg)
    void sysMsgClick() {
        startFragment(new SysmsglistFragment());
    }

    @OnClick(R.id.layout_setting_setting)
    void settingClick() {
        startFragment(new SettingSwithFragment());
    }

    @OnClick(R.id.layout_setting_faq)
    void faqClick() {
        WebFragment.start(this, "FAQ&お問い合わせ", "http://salient.jetkys.com/index.php/faq_a");
    }

    @OnClick(R.id.layout_setting_help)
    void helpClick() {
        WebFragment.start(this, "ヘルプ", "http://salient.jetkys.com/index.php/help_a");
    }

    @OnClick(R.id.layout_setting_contract)
    void contractClick() {
        WebFragment.start(this, "利用規約", "http://salient.jetkys.com/index.php/rules_a");
    }

    @OnClick(R.id.layout_setting_wtf)
    void wtfClick() {
        WebFragment.start(this, "プライバシーポリシー", "http://salient.jetkys.com/index.php/privacy_a");
    }

    @Bind(R.id.iv_member_avatar)
    ImageView avatarIv;

    @OnClick(R.id.iv_member_avatar)
    void photoClick() {
        mPhotoPicker.startPick();
    }

    private PhotoPicker mPhotoPicker;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        String photoPath = mPhotoPicker.onActivityResult(requestCode, resultCode, data);
        if (!TextUtils.isEmpty(photoPath)) { // handled
            photoPicked(photoPath);
            return;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void photoPicked(String path) {
        uploadPhoto(path);
    }

    private void uploadPhoto(String path) {
        mNetManager.uploadPhoto(path, new Callback<UploadPhotoBean>() {
            @Override
            protected void onSucceed(UploadPhotoBean result) {
                setUserImage(mCurrType, result.imageid);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void setUserImage(String type, final String imgid) {
        mNetManager.setUserImage(type, imgid, new Callback<Void>() {
            @Override
            protected void onSucceed(Void result) {
                dismissLoading();

                asyncLoadImage(imgid, avatarIv);
                mAccountManager.setImageUrl(imgid);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
    }

    @Bind(R.id.iv_msg_new)
    ImageView newMsgSign;

    @OnClick(R.id.btn_setting_editprofile)
    void editProfileClick() {
        if (mUserInfo == null) return;

        Bundle bundle = new Bundle();
        bundle.putString("title", mUserInfo.nickname);
        bundle.putSerializable("user", mUserInfo);
        bundle.putSerializable("type", mCurrType);

        ProfileEditFragment f = new ProfileEditFragment();
        f.setArguments(bundle);
        startFragmentForResult(f, 1);
    }

    @Override
    protected void onFragmentResult(int requestCode, int resultCode, Bundle datas) {
        super.onFragmentResult(requestCode, resultCode, datas);

        if (requestCode == 1 && resultCode == RESULT_OK) {
            refresh(mCurrType);
        }
    }

    @Bind(R.id.btn_setting_prevprofile)
    View mProfieView;

    @OnClick(R.id.btn_setting_prevprofile)
    void prevProfileClick() {
        Bundle bundle = new Bundle();
        bundle.putBoolean("preview", true);
        bundle.putString("id", mAccountManager.concierge_id);
        bundle.putString("name", mUserInfo.nickname);

        MemberFragment fragment = new MemberFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    @Bind(R.id.tv_member_point)
    TextView mPointTv;
    @Bind(R.id.tv_member_name)
    TextView mNameTv;
    @Bind(R.id.tv_member_character)
    TextView mCharaTv;
    @Bind(R.id.layout_setting_coupon)
    View couponLayout;

    @Bind(R.id.tv_member_coupon0)
    TextView mCouponTv0;
    @Bind(R.id.tv_member_coupon1)
    TextView mCouponTv1;

    private UserInfo mUserInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPhotoPicker = new PhotoPicker(this, mActivity);
    }

    @Override
    protected void initViews(Bundle bundle) {
        super.initViews(bundle);

        mCharaTv.setText("ID:" + mAccountManager.user_id);

        setDatas();
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        asyncLoadImage(mAccountManager.profile_image_url, avatarIv);
        mNameTv.setText(mAccountManager.nickname);

        refresh("1");
    }

    private void refresh(final String type) {
        mNetManager.getUserInfo(type, new Callback<UserInfo>() {
            @Override
            protected void onSucceed(UserInfo result) {
                dismissLoading();

                mUserInfo = result;
                setDatas();

                if ("1".equals(type)) {
                    mCharaTv.setText("ID:" + mAccountManager.user_id);
                    mProfieView.setVisibility(View.INVISIBLE);
                    couponLayout.setVisibility(View.VISIBLE);
                } else if ("2".equals(type)) {
                    mCharaTv.setText("ID:" + mAccountManager.concierge_id);
                    mProfieView.setVisibility(View.VISIBLE);
                    couponLayout.setVisibility(View.GONE);
                }

                setType(type);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void setDatas() {
        if (mUserInfo == null) return;

        asyncLoadImage(mUserInfo.profile_image_url, avatarIv);
        String point = ("".equals(mUserInfo.pt) || "0".equals(mUserInfo.pt)) ?
                "0pt" : mUserInfo.pt + "pts";
        mPointTv.setText(point);
        mNameTv.setText(mUserInfo.nickname);

        String tkt0 = "".equals(mUserInfo.diagnosis_ticket) ? "0" : mUserInfo.diagnosis_ticket;
        mCouponTv0.setText(tkt0 + "枚");
        String tkt1 = "".equals(mUserInfo.chat_ticket) ? "0" : mUserInfo.chat_ticket;
        mCouponTv1.setText(tkt1 + "枚");

        newMsgSign.setVisibility("1".equals(mUserInfo.if_new) ? View.VISIBLE : View.INVISIBLE);

        if (!TextUtils.isEmpty(mAccountManager.concierge_id)) {
            // 是专家
            mProfieView.setVisibility(View.VISIBLE);
            mTab1.setVisibility(View.VISIBLE);
        }
    }
}
