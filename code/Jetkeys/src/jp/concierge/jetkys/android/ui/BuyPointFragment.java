package jp.concierge.jetkys.android.ui;

import android.os.Bundle;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;

/**
 * Created by WilliZ on 16/03/31.
 */
public class BuyPointFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_buypoint;
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick() {
        finish();
    }

    @Bind(R.id.rgroup_buypoint_items)
    RadioGroup mItemsRadioGroup;

    @OnClick(R.id.btn_submit)
    void buyClick() {
        String point = "";
        int checkedId = mItemsRadioGroup.getCheckedRadioButtonId();
        switch (checkedId) {
            case R.id.rb_buypoint_1000:
                point = "1000";
                break;
            case R.id.rb_buypoint_2000:
                point = "2000";
                break;
            case R.id.rb_buypoint_5000:
                point = "5000";
                break;
            case R.id.rb_buypoint_10000:
                point = "10000";
                break;
        }

        if ("".equals(point)) return;

        gotoBuy(point);
    }

    private void gotoBuy(String point) {
        toast("Buy [" + point + "]");
    }

    @Bind(R.id.tv_point)
    TextView mPointTv;

    private String point;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        point = getArguments().getString("point");
    }

    @Override
    protected void initViews(Bundle bundle) {
        super.initViews(bundle);

        mPointTv.setText(point);
    }
}
