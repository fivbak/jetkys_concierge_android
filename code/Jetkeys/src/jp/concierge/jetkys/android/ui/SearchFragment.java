package jp.concierge.jetkys.android.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.GetUpdate;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.manager.AccountManager;
import jp.concierge.jetkys.android.manager.NetManager;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.utils.ConstData;
import jp.concierge.jetkys.android.utils.PhotoUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by WilliZ on 16/03/19.
 */
public class SearchFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_search;
    }

    @Bind({R.id.cb_search_0, R.id.cb_search_1, R.id.cb_search_2,
            R.id.cb_search_3, R.id.cb_search_4, R.id.cb_search_5,
            R.id.cb_search_6, R.id.cb_search_7, R.id.cb_search_8,
            R.id.cb_search_9, R.id.cb_search_10, R.id.cb_search_11,
            R.id.cb_search_12, R.id.cb_search_13,})
    List<CheckBox> mTypeCbList;
    private ArrayList<Integer> mCheckedTypeList;

    @Bind(R.id.rgroup_gender)
    RadioGroup mGenderRgroup;

    @OnClick(R.id.btn_search)
    void searchClick() {
        String gender = ViewPicker.getGender(mGenderRgroup);
        String type = ConstData.getTypeCheckedIds(mCheckedTypeList);

        Bundle bundle = new Bundle();
        bundle.putString("gender", gender);
        bundle.putString("cat", type);

        ConciergeListFragment fragment = new ConciergeListFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCheckedTypeList = new ArrayList<>();
    }

    private boolean mCheckItemInitingLock;

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        int size = ConstData.sTypeLists.size();
        for (int i = 0; i < size; i++) {
            String type = ConstData.sTypeLists.get(i);
            CheckBox cb = mTypeCbList.get(i);
            cb.setText(type);

            final int pos = i;
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (mCheckItemInitingLock) return;

                    if (isChecked && !mCheckedTypeList.contains(pos)) {
                        mCheckedTypeList.add(Integer.valueOf(pos));
                    } else {
                        mCheckedTypeList.remove(Integer.valueOf(pos));
                    }

                    Collections.sort(mCheckedTypeList);
                }
            });
        }

        mCheckItemInitingLock = true;
        for (int index : mCheckedTypeList) {
            mTypeCbList.get(index).setChecked(true);
        }
        mCheckItemInitingLock = false;
    }
}
