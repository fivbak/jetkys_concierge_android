package jp.concierge.jetkys.android.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.OnClick;
import jp.concierge.jetkys.android.PicBrowseActivity;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.MailBean;
import jp.concierge.jetkys.android.beans.UploadPhotoBean;
import jp.concierge.jetkys.android.net.thread.Callback;
import jp.concierge.jetkys.android.ui.utils.DisplayUtil;
import jp.concierge.jetkys.android.ui.utils.PhotoPicker;
import jp.concierge.jetkys.android.ui.utils.ZImageLoader;

import java.util.Calendar;

/**
 * Created by WilliZ on 16/03/20.
 */
public class MailPrevFragment extends BaseFragment {

    @Override
    protected int layout() {
        return R.layout.fragment_mail_preview;
    }

    @OnClick(R.id.btn_submit)
    void submitClick() {
        new AlertDialog.Builder(mActivity)
                .setMessage(R.string.hint_confirm_content_ok)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        submit0();
                    }
                }).setNegativeButton(R.string.cancel, null).show();
    }

    private void submit0() {
        if (!TextUtils.isEmpty(mMailBean.image_url)) {
            uploadPic();
        } else {
            submit("");
        }
    }

    private void uploadPic() {
        mNetManager.uploadPhoto(mMailBean.image_url, new Callback<UploadPhotoBean>() {
            @Override
            protected void onSucceed(UploadPhotoBean result) {
                submit(result.imageid);
            }

            @Override
            protected void onError(Object object) {
                dismissLoading();
            }
        });
        showLoading();
    }

    private void submit(String imgurl) {
        mNetManager.sendMail(
                mMailBean.concierge_id,
                mMailBean.title,
                mMailBean.message,
                imgurl,
                mMailBean.conversation_id,
                mMailBean.diagnoses_id, new Callback<Void>() {
                    @Override
                    protected void onSucceed(Void result) {
                        dismissLoading();
                        toast(getString(R.string.hint_submit_success));

                        finishToMailList();
                    }

                    @Override
                    protected void onError(Object object) {
                        dismissLoading();
                    }
                });
        showLoading();
    }

    public void finishToMailList() {
        mActivity.sendBroadcast(new Intent(MailListFragment.FILTER_MAIL_NEED_REFRESH));

        mActivity.toMailList();
    }

    @OnClick(R.id.btn_cancel)
    void cancelClick() {
        finish();
    }

    @Bind(R.id.tv_mail_title)
    TextView mTitleTv;
    @Bind(R.id.tv_mail_content)
    TextView mContentTv;
    @Bind(R.id.iv_mail_camera)
    ImageView mCameraIv;
    @Bind(R.id.iv_photo)
    ImageView mPhotoIv;

    @OnClick(R.id.iv_photo)
    void onPicClick() {
        if (TextUtils.isEmpty(mMailBean.image_url)) return;

        Intent intent = new Intent();
        intent.setClass(mActivity, PicBrowseActivity.class);
        intent.putExtra("url", "file://" + mMailBean.image_url);
        startActivity(intent);
    }

    private MailBean mMailBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        mMailBean = (MailBean) bundle.getSerializable("mail");
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        needBackFinish();

        setDatas();
    }

    private void setDatas() {
        mTitleTv.setText(mMailBean.title);
        mContentTv.setText(mMailBean.message);

        boolean hasAttachment = !TextUtils.isEmpty(mMailBean.image_url);
        mCameraIv.setImageResource(hasAttachment ?
                R.drawable.ico_camera_with_attachment : R.drawable.ic_title_camera);
        mPhotoIv.setVisibility(hasAttachment ? View.VISIBLE : View.GONE);
        String pic = "file://" + mMailBean.image_url;
        ZImageLoader.asyncLoadImage(pic, mPhotoIv);
    }
}
