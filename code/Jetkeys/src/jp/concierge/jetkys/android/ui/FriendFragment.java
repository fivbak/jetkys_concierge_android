package jp.concierge.jetkys.android.ui;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import butterknife.Bind;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshExpandableListView;
import jp.concierge.jetkys.android.R;
import jp.concierge.jetkys.android.beans.FriendBean;
import jp.concierge.jetkys.android.beans.base.BaseFriendListEntity;
import jp.concierge.jetkys.android.net.thread.Callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by WilliZ on 16/03/21.
 */
public class FriendFragment extends BaseFragment {
    @Override
    protected int layout() {
        return R.layout.fragment_friend;
    }

    private List<String> GROUPS;

    private List<FriendBean> mGroupList0;
    private List<FriendBean> mGroupList1;

    private Map<String, List<FriendBean>> mDataMap;

    private LayoutInflater mInflater;

    private ExpandListAdapter mExpandListAdapter;

    @Bind(R.id.pulllistview)
    PullToRefreshExpandableListView mPullExpadListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataMap = new HashMap<>();

        GROUPS = new ArrayList<>();
        GROUPS.add("マイサリエンシー");
        mGroupList0 = new ArrayList<>();
        mDataMap.put(GROUPS.get(0), mGroupList0);

        // 是专家才显示下面的
        if (!TextUtils.isEmpty(mAccountManager.concierge_id)) {
            GROUPS.add("マイユーザー");
            mGroupList1 = new ArrayList<>();
            mDataMap.put(GROUPS.get(1), mGroupList1);
        }

        mExpandListAdapter = new ExpandListAdapter();
    }

    @Override
    protected void initViews(Bundle savedBundle) {
        super.initViews(savedBundle);

        mInflater = LayoutInflater.from(mActivity);
        mPullExpadListView.getRefreshableView().setSelector(new ColorDrawable(Color.TRANSPARENT));
        mPullExpadListView.getRefreshableView().setAdapter(mExpandListAdapter);
        mPullExpadListView.getRefreshableView().setGroupIndicator(null);
        mPullExpadListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ExpandableListView>() {

            @Override
            public void onRefresh(PullToRefreshBase<ExpandableListView> refreshView) {
                refresh();
            }
        });

        if (savedBundle != null) {
            int first = savedBundle.getInt("first", 0);
            mPullExpadListView.getRefreshableView().setSelection(first);
        }
    }

    @Override
    protected void saveBundle(Bundle bundle) {
        super.saveBundle(bundle);

        int first = mPullExpadListView.getRefreshableView().getFirstVisiblePosition();
        bundle.putInt("first", first);
    }

    @Override
    protected void onFirstTimeInit() {
        super.onFirstTimeInit();

        refresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mPullExpadListView.onRefreshComplete();
    }

    private void refresh() {
        mNetManager.getFriendList(new Callback<BaseFriendListEntity>() {
            @Override
            protected void onSucceed(BaseFriendListEntity result) {
                mPullExpadListView.onRefreshComplete();

                mGroupList0.clear();
                mGroupList0.addAll(result.concierges);

                if (mGroupList1 != null) {
                    mGroupList1.clear();
                    mGroupList1.addAll(result.friend);
                }

                mExpandListAdapter.notifyDataSetChanged();

                if (mGroupList0.size() > 0) {
                    mPullExpadListView.getRefreshableView().expandGroup(0);
                } else if (mGroupList1 != null && mGroupList1.size() > 0) {
                    mPullExpadListView.getRefreshableView().expandGroup(1);
                }
            }

            @Override
            protected void onError(Object object) {
                mPullExpadListView.onRefreshComplete();
            }
        });
    }

    private void test() {
        for (int i = 0; i < 8; i++) {
            FriendBean bean = new FriendBean();
            mGroupList0.add(bean);
        }
        for (int i = 0; i < 5; i++) {
            FriendBean bean = new FriendBean();
            mGroupList1.add(bean);
        }

        mExpandListAdapter.notifyDataSetChanged();
    }

//    private void gotoMember(FriendBean item) {
//        Bundle bundle = new Bundle();
//        bundle.putString("id", item.id);
//
//        MemberFragment fragment = new MemberFragment();
//        fragment.setArguments(bundle);
//        startFragment(fragment);
//    }

    private void gotoMail(int groupId, FriendBean item) {
        Bundle bundle = new Bundle();
        if (groupId == 0) {
            bundle.putString("cid", item.id);
            bundle.putString("name", item.getName());
        } else {
            bundle.putString("uid", item.id);
            bundle.putString("name", item.nickname);
        }

        MailListInnerFragment fragment = new MailListInnerFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    private void gotoMessage(int groupId, FriendBean item) {
        Bundle bundle = new Bundle();
        if (groupId == 0) {
            bundle.putString("cid", item.id);
            bundle.putString("title", item.getName());
        } else {
            bundle.putString("uid", item.id);
            bundle.putString("title", item.nickname);
        }

        ChatFragment fragment = new ChatFragment();
        fragment.setArguments(bundle);
        startFragment(fragment);
    }

    private class ExpandListAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return GROUPS.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            String group = getGroup(groupPosition);
            List<FriendBean> list = mDataMap.get(group);
            return list.size();
        }

        @Override
        public String getGroup(int groupPosition) {
            String group = GROUPS.get(groupPosition);
            return group;
        }

        @Override
        public FriendBean getChild(int groupPosition, int childPosition) {
            String group = getGroup(groupPosition);
            List<FriendBean> list = mDataMap.get(group);
            return list.get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            GroupHolder h = null;
            if (convertView == null) {
                h = new GroupHolder();
                convertView = mInflater.inflate(R.layout.item_list_friend_group, null);
                h.title = (TextView) convertView.findViewById(R.id.tv_title);
                h.sign = (ImageView) convertView.findViewById(R.id.iv_list_sign);

                convertView.setTag(h);
            } else {
                h = (GroupHolder) convertView.getTag();
            }

            String group = getGroup(groupPosition);
            int childrenCount = getChildrenCount(groupPosition);
            h.title.setText(group + "(" + childrenCount + ")");
            h.sign.setImageResource(isExpanded ? R.drawable.ic_expand_open : R.drawable.ic_expand_close);

            return convertView;
        }

        private class GroupHolder {
            public TextView title;
            public ImageView sign;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            ChildHolder h = null;
            if (convertView == null) {
                h = new ChildHolder();
                convertView = mInflater.inflate(R.layout.item_list_friend, null);
                h.avatar = (ImageView) convertView.findViewById(R.id.iv_item_list_avatar);
                h.title = (TextView) convertView.findViewById(R.id.tv_item_list_name);
                h.desc = (TextView) convertView.findViewById(R.id.tv_item_list_desc);
                h.msg = convertView.findViewById(R.id.layout_list_msg);
                h.mail = convertView.findViewById(R.id.layout_list_mail);
                h.msgSign = convertView.findViewById(R.id.iv_msg_sign);
                h.mailSign = convertView.findViewById(R.id.iv_mail_sign);

                convertView.setTag(h);
            } else {
                h = (ChildHolder) convertView.getTag();
            }

            final int groupId = groupPosition;
            final FriendBean item = getChild(groupPosition, childPosition);
            h.mail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoMail(groupId, item);
                }
            });
            h.msg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotoMessage(groupId, item);
                }
            });

            String nick = "";
            if (groupPosition == 0) {
                nick = item.getName();
            } else if (groupPosition == 1) {
                nick = item.nickname;
            }
            h.title.setText(nick);
            asyncLoadImage(item.image, h.avatar);

            h.mailSign.setVisibility("1".equals(item.if_mail) ? View.VISIBLE : View.GONE);
            h.msgSign.setVisibility("1".equals(item.if_message) ? View.VISIBLE : View.GONE);

            return convertView;
        }

        private class ChildHolder {
            public ImageView avatar;
            public TextView title;
            public TextView desc;
            public View msg;
            public View mail;
            public View msgSign;
            public View mailSign;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        @Override
        public void onGroupExpanded(int groupPosition) {
            super.onGroupExpanded(groupPosition);
        }
    }
}
