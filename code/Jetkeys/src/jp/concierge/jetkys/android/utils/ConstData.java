package jp.concierge.jetkys.android.utils;

import java.util.ArrayList;
import java.util.List;

public class ConstData {
    public static boolean sIsAppFree = false;

    public static final String TAG = ConstData.class.getSimpleName();

    public static final int PAGE_FIRST = 1;

    public static List<String> sTypeLists;
    public static List<String> sTypeIdLists;

    static {
        sTypeLists = new ArrayList<>();
        sTypeIdLists = new ArrayList<>();
        sTypeIdLists.add("1");
        sTypeLists.add("恋愛、婚活、結婚");
        sTypeIdLists.add("2");
        sTypeLists.add("妊娠、子育て");
        sTypeIdLists.add("3");
        sTypeLists.add("夫婦関係、離婚");
        sTypeIdLists.add("4");
        sTypeLists.add("仕事、人間関係");
        sTypeIdLists.add("5");
        sTypeLists.add("お金、住まい、健康");
        sTypeIdLists.add("6");
        sTypeLists.add("教育、進学、勉強");
        sTypeIdLists.add("7");
        sTypeLists.add("家族、子供");
        sTypeIdLists.add("8");
        sTypeLists.add("介護、相続");
        sTypeIdLists.add("9");
        sTypeLists.add("旅行、趣味");
        sTypeIdLists.add("10");
        sTypeLists.add("隣人、ネット");
        sTypeIdLists.add("11");
        sTypeLists.add("美容、ダイエット");
        sTypeIdLists.add("12");
        sTypeLists.add("メンタル、心理");
        sTypeIdLists.add("13");
        sTypeLists.add("芸能、スポーツ");
        sTypeIdLists.add("14");
        sTypeLists.add("占い、カウンセリング");
    }

    public static String getTypeCheckedIds(List<Integer> checkedList) {
        StringBuilder sb = new StringBuilder();

        int len = checkedList.size();
        for (int i = 0; i < len; i++) {
            int index = checkedList.get(i);
            String id = sTypeIdLists.get(index);
            sb.append(id);
            if (i < len - 1) {
                sb.append(",");
            }
        }

        return sb.toString();
    }

    public static int findIndex(String cat) {
        int size = sTypeIdLists.size();
        for (int i = 0; i < size; i++) {
            String idStr = sTypeIdLists.get(i);

            if (idStr.equals(cat)) return i;
        }
        return -1;
    }
}