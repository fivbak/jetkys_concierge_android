package jp.concierge.jetkys.android.utils;

import java.io.File;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.ClipboardManager;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import jp.concierge.jetkys.android.R;

public class Utils {

    public static void toast(Context context, CharSequence text) {
        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
    }

    public static String arrayToString(String[] ids) {
        String string = "";
        for (String id : ids) {
            string += (id + ",");
        }

        return string.substring(0, string.length() - 1);
    }

    public static String listToString(List<String> ids) {
        String string = "";
        for (String id : ids) {
            string += (id + ",");
        }

        return string.substring(0, string.length() - 1);
    }

    public static void copyToClipBoard(Context context, CharSequence s) {
        ClipboardManager clip = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        clip.getText(); // 粘贴
        clip.setText(s); // 复制

//        toast(context, context.getString(R.string.success_copy));
    }

    public static void disableKeyBoard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view != null) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}