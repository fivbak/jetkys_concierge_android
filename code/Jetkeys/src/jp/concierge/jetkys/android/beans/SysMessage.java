package jp.concierge.jetkys.android.beans;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Created by WilliZ on 16/03/21.
 */
public class SysMessage implements Serializable {

    private static final long serialVersionUID = 8849089851196680309L;

    public String id;
    public String title;
    public String text;
    public String date;
    public String status;
}
