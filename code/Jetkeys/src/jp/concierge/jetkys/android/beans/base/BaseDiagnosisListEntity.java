package jp.concierge.jetkys.android.beans.base;

import jp.concierge.jetkys.android.beans.Diagnosis;

import java.util.List;

/**
 * Created by WilliZ on 16/03/30.
 */
public class BaseDiagnosisListEntity {
    public String limit_hour;
    public List<Diagnosis> diagnoses;
}
