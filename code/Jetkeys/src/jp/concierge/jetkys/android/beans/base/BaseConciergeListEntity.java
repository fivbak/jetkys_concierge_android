package jp.concierge.jetkys.android.beans.base;

import jp.concierge.jetkys.android.beans.Concierge;

import java.util.List;

/**
 * Created by WilliZ on 16/03/30.
 */
public class BaseConciergeListEntity {
    public List<Concierge> concierges;
}
