
package jp.concierge.jetkys.android.beans;

import android.text.TextUtils;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Message implements Serializable {
    public static final String TAG = Message.class.getSimpleName();
    private static final long serialVersionUID = 622804320171388037L;

    public static final String TEXT = "1";
    public static final String PICTURE = "2";

    public String id;
    public String nickname = "";
    @SerializedName("image")
    public String uavatar = "";
    public String message = "";
    @SerializedName("msg_type")
    public String type = TEXT;
    public String diagnosis_id = "";
    public String concierge_id = "";
    public String user_id = "";
    public String status = "";
    public String from_id = "";
    public String to_id = "";
    public String time = "";
    public String vector = "";
    public String conversation_id;

    public Message() {
    }
}
