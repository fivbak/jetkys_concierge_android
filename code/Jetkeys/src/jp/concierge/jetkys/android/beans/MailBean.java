package jp.concierge.jetkys.android.beans;

import java.io.Serializable;

/**
 * Created by WilliZ on 16/03/30.
 */
public class MailBean implements Serializable {
    private static final long serialVersionUID = 900779480540356659L;

    public String id;
    public String concierge_id;
    public String nickname;
    public String profile_image_url; // 发信人头像
    public String image_url; // 邮件附件图片
    public String title;
    public String message;
    public String attachment; // 1:有附件 0:没附件
    public String status; // 1:已读 0:未读
    public String send_ts; // 发送时间
    public String conversation_id; // 会话id
    public String diagnoses_id; // 诊断id
    public String from_id; // 谁发的
}
