package jp.concierge.jetkys.android;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by WilliZ on 16/03/20.
 */
public class Settings {
    public static final String SP_FILE = "setting";

    public static final String STATUS_ICON = "status_icon";
    public static final String SOUND = "sound";
    public static final String VIBRATION = "vibration";
    public static final String MESSAGE_NOTIF = "message_notification";
    public static final String PAY_CONFIRM_SEND_MAIL = "pay_confirm_send_mail";
    public static final String PAY_CONFIRM_SEND_MESSAGE = "pay_confirm_send_message";
    public static final String PAY_CONFIRM_SEE_PICTURE = "pay_confirm_picture";

    public static HashMap<String, Boolean> sSettingsMap = new HashMap<>();

    public static SharedPreferences sSharedPreferences;

    public static void init(Context context) {
        sSharedPreferences = context.getSharedPreferences(SP_FILE, Context.MODE_PRIVATE);
        Map<String, ?> map = sSharedPreferences.getAll();

        if (map == null || map.size() == 0) {
            resetDefault();
        } else {
            for (String key : map.keySet()) {
                boolean value = (Boolean) map.get(key);
                sSettingsMap.put(key, value);
            }
        }
    }

    public static void resetDefault() {
        sSettingsMap.put(STATUS_ICON, true);
        sSettingsMap.put(SOUND, true);
        sSettingsMap.put(VIBRATION, true);
        sSettingsMap.put(MESSAGE_NOTIF, true);
        sSettingsMap.put(PAY_CONFIRM_SEND_MAIL, true);
        sSettingsMap.put(PAY_CONFIRM_SEND_MESSAGE, true);
        sSettingsMap.put(PAY_CONFIRM_SEE_PICTURE, true);

        SharedPreferences.Editor editor = sSharedPreferences.edit();
        for (String key : sSettingsMap.keySet()) {
            boolean value = sSettingsMap.get(key);
            editor.putBoolean(key, value);
        }
        editor.apply();
    }

    public static void save(String key, boolean value) {
        sSettingsMap.put(key, value);

        sSharedPreferences.edit().putBoolean(key, value).apply();
    }

    public static boolean get(String key) {
        return sSettingsMap.get(key);
    }
}
