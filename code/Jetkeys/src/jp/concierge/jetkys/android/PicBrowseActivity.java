package jp.concierge.jetkys.android;

import android.app.Activity;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import jp.concierge.jetkys.android.ui.utils.ZImageLoader;

public class PicBrowseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pic_browse);

		ImageView mainIv = (ImageView) findViewById(R.id.iv_pic_browse_main);

		Bundle bundle = getIntent().getExtras();
		String url = bundle.getString("url");
		String thumb = bundle.getString("thumb");

		if (TextUtils.isEmpty(url) && TextUtils.isEmpty(thumb)) {
			finish();
			return;
		}

		if (!TextUtils.isEmpty(thumb)) {
			ZImageLoader.asyncLoadImage(thumb, mainIv);
		}

		ZImageLoader.asyncLoadImage(url, mainIv, ZImageLoader.sSimpleOptions,
				new ImageLoadingListener() {

					@Override
					public void onLoadingStarted(String imageUri, View view) {
						// toast("loading...");
					}

					@Override
					public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

					}

					@Override
					public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

					}

					@Override
					public void onLoadingCancelled(String imageUri, View view) {

					}
				});
	}
}
